" minimal vimrc - bitbucket.org/mga_oshima/minivimrc

set nocompatible
set title
set hidden
set confirm
set backspace=indent,eol,start
set number
set ruler
set nocursorline
set laststatus=2
set nowrap
set ignorecase
set smartcase
set gdefault
set incsearch
set hlsearch
set wrapscan
set nrformats=
set autoindent
set smartindent
set smarttab
set expandtab
set tabstop=2
set shiftwidth=2
set list listchars=tab:▸\ "
set wildmode=list:longest,full

syntax on
filetype plugin indent on

nmap <C-h> :bp<CR>
nmap <C-l> :bn<CR>
nmap j gj
nmap k gk
